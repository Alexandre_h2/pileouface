import random as rd

pf_array = ["pile", "face"]

while True:
    choice = input("Pile ou face ?")
    if choice.lower() in pf_array:
        break
pile_ou_face = rd.choice(pf_array)
if pile_ou_face == "pile" and choice.lower() == "pile" or pile_ou_face == "face" and choice.lower() == "face":
    print("Gagné!")
else:
    print("Perdu !")
